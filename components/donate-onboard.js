import React, { useState, useEffect } from 'react'
import { ethers } from 'ethers'
import Router from 'next/router'
import Web3 from 'web3'


const donationAddress = '0xFfA55b403bF021D92F3C166a1280cDfCcBc8D022'

const web3 = new Web3(process.env.NEXT_PUBLIC_ALCHEMY_ENDPOINT)

export default function MaticPayment({ numMatic, reference  }){
  const [donoraddress, setDonorAdddress] = useState()
  // const [maticPrice, setMaticPrice] = useState('25')
  const [showChain, setShowChain] = useState(false)
  const [txSuccess, setTxSucess] = useState(false)
  const [txProgress, setTxProgress] = useState(false)
  const [ providerSign, setProviderSign] = useState(null)
  const [error, setError] = useState(false)
  const [numberKit, setNumberKit] = useState(1)
 
  async function connect() {
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    let accounts = await provider.send("eth_requestAccounts", []);
    let account = accounts[0];
    provider.on('accountsChanged', function (accounts) {
        account = accounts[0];// Print new address
    });

    const signer = provider.getSigner();

   

    const address = await signer.getAddress();
    setDonorAdddress(address)

    if (!address || !signer || !provider) {
      console.log("ERROR couldn't connect to metamask")
    }
    
    const { chainId } = await provider.getNetwork();
    if(chainId !== 80001){
      console.log('Please switch to matic chain')
      setShowChain(true)
    } else {
      setProviderSign(signer)
      setShowChain(false)
      // sendTransation(signer)
    }
}

async function sendTransation(signer){
  try {
    //send a transaction with the ethers provider
    const txn = await signer.sendTransaction({
      to: donationAddress,
      // value: 1000000000000000 //0.001 
      value: ethers.utils.parseEther(String(numberKit * numMatic))
    })
    setTxProgress(true)
    const receipt = await txn.wait()
    setTxSucess(true)
    setError(false)
    Router.reload(window.location.pathname)
  }
  catch (error) {
    console.error(error);
    setError(true)
  }
}

  return (
    <div ref={reference}>
      <h3 className="text-xl font-semibold">Essentials</h3>
      <p className="text-blueGray-400 leading-loose">An Essentials School Kit pack includes a school backpack, a set of notebooks and other essential educational materials for a school kid.</p>
    
      <small className="text-[10px]"> $20 in $MATIC Token</small>
      <br/>
      {showChain ? <small className='text-red-600'>Please switch to the Matic Mumbai blockchain</small> : null}
      
      {providerSign ?
        <div>
          <input
        type="number"
        name={name}
        value={numberKit}
        onChange={e => setNumberKit(Number(e.target.value))}
        min={1}
        className="w-12 border-none focus:ring-0 text-center bg-gray-200"
      />
      <p> You would like to donate: ${20 * numberKit} ({ (numberKit * numMatic).toFixed(4) } MATIC)</p> 
        </div>
      : null }
    
      <button className=" flex bg-blue-600 hover:bg-blue-700 text-white font-bold py-3 px-2 w-full lg:w-full xl:w-3/5 justify-center rounded cursor-pointer"
        onClick={() => 
        !providerSign ? connect() : sendTransation(providerSign)}
      >
         {!providerSign ? `Connect Wallet` : `Gift A Kit Now`}  
      </button>
      
      { txProgress ? 
        <div className="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3 mt-2" role="alert">
          <p className="font-bold">Transaction in progress!</p>
          <p className="text-sm">We reallt appreciate your kind gesture.</p>
        </div>
        :null }

      { txSuccess ? 
        <div className="bg-blue-100 border-t border-b border-green-500 text-blue-700 px-4 py-3 mt-2" role="alert">
          <p className="font-bold">Transaction successful!</p>
          <p className="text-sm">Thanks for donating.</p>
        </div>
        : null}
      {
        error === true ? <small className='text-red-600'>Please try again. Make sure you have sufficient funds</small> : null
      }

    </div>
  )
}