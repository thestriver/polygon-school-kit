/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    "loader": "akamai",
    "path": "",
    domains: ['thehelpmatesfoundation.com', 'cdn.tailgrids.com', 'kutty.netlify.app', 'schoolkit-frontend.vercel.app', 'images.pexels.com', 'cdn.pixabay.com', 'images.unsplash.com']
  },
  trailingSlash: true,
  swcMinify: true,
  webpack: (config) => {
    // this will override the experiments
    config.experiments = { ...config.experiments, ...{ topLevelAwait: true }};
    // this will just update topLevelAwait property of config.experiments
    // config.experiments.topLevelAwait = true 
    return config;
  },
}

module.exports = nextConfig
