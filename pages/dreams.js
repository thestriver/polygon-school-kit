const Dreams = () => {
    return (
        <div className="w-full lg:w-1/2">
              <div className="mb-12 lg:mb-0 pb-12 lg:pb-0 border-b lg:border-b-0">
                <h2 className="mb-4 text-4xl lg:text-5xl font-bold font-heading">Help School Kids Achieve Their Dreams</h2>
                <p className="mb-4 leading-loose text-blueGray-400">
                    There is a huge chance Crypto has had a huge influence on your life. And we&apos;re hoping you can help make it changed others too. </p>
                    
                <p className="mb-4 leading-loose text-blueGray-400"> There are over <span className="font-bold" >200 million out of school children globally</span>
                    and those still in school lack appropriate learning materials to truly achieve their goals.
                </p>
                    
                <p className="mb-4 leading-loose text-blueGray-400"> Over the past couple of years, I&apos;ve participated in efforts to donate proper school kits to needy school kids in sub-Saharan Africa especially 
                    in places where millions are living in extreme poverty but there&apos;s still so much to be done. 
                </p>
              <p className="mb-4 leading-loose text-blueGray-400">
                    With Gift A School Kit, <b>we can help make a difference</b> . Help these kids fulfil their right to education. Help them achieve their dreams. 
                    Gift A School Kit Today.
                    
                    
                </p>
                
            </div>
        </div>
    )
}

export default Dreams