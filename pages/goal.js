import React, { useState, useEffect } from 'react'
import { ethers } from 'ethers'

const donationAddress = '0xFfA55b403bF021D92F3C166a1280cDfCcBc8D022'

const Goal = ({ maticPrice }) => {
    const [totalBalance, setTotalBalance ] = useState(0)
    useEffect(() => {
        const provider = new ethers.providers.JsonRpcProvider("https://matic-mumbai.chainstacklabs.com")
        const balance = provider.getBalance(donationAddress)
        balance.then((result) => {
        setTotalBalance(Number(ethers.utils.formatEther(result)) * maticPrice)
        })
      // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [])
    return (
        <section className="text-gray-600 body-font">
            <div className="container px-5 mx-auto">
                <div className="relative pt-1 pb-8 lg:mx-32">
                    <div className="flex mb-2 items-center justify-between">
                        <div>
                        <span className="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-blue-600 bg-blue-200">
                            Quarterly Goal (100 School Kits): 
                        </span>
                        </div>
                        <div className="text-right">
                        <span className="text-xs font-semibold inline-block text-blue-600">
                            {(totalBalance.toFixed(2) / 20)/10}%
                        </span>
                        </div>
                    </div>
                    <div className="overflow-hidden h-2 mb-4 text-xs flex rounded bg-blue-200">
                        <div 
                        style={{ width: `${(totalBalance.toFixed(2) / 20)/10}%` }}
                        className="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-blue-600">
                        </div>
                    </div>
                </div>
                    <div className="flex flex-wrap -m-4 text-center">
                        <div className="p-4 lg:w-1/3 w-1/2  md:border-r">
                            <h2 className="text-4xl font-bold lg:text-5xl xl:text-6xl">{ totalBalance.toFixed(2) / 20 }</h2>
                            <p className="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base">Total School Kits Donated</p>
                        </div>
                        <div className="p-4 lg:w-1/3 w-1/2 md:border-r">
                            <h2 className="text-4xl font-bold lg:text-5xl xl:text-6xl">${totalBalance.toFixed(2)}</h2>
                            <p className="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base"> Total Money Raised</p>
                        </div>
                        <div className="p-4 lg:w-1/3 w-1/2" >
                            {/* <h2 className="text-4xl font-bold lg:text-5xl xl:text-6xl">0</h2>
                            <p className="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base">Total No of School Kids</p> */}
                        </div>
                    </div>
                    
                </div>
            </section>
    )
}

export default Goal;