/* eslint-disable @next/next/no-img-element */
import Image from 'next/image'

const Hero = ({ reference, click }) => {
    return(
        <div className="relative bg-white overflow-hidden my-12">
            <div className="pt-16 pb-80 sm:pt-24 sm:pb-40 lg:pt-40 lg:pb-48">
                <div className="relative 2xl:mx-44 xl:mx-16 lg:mx-10 px-4 sm:px-6 sm:static">
                    <div ref={reference} className="sm:max-w-lg xl:max-w-xl">
                        <h1 className="text-6xl font font-extrabold tracking-tight text-gray-900 sm:text-6xl">
                        Gift A School Kit
                        </h1>
                        <p className="mt-4 text-xl text-gray-500">
                        Gift A School Kit is a web3 project that provides underpriviledged school kids 
                        access to basic educational materials.
                        </p>
                        <a onClick={click} className="cursor-pointer mt-4 inline-block text-center bg-blue-600 border border-transparent rounded-md py-3 px-8 font-medium text-white hover:bg-blue-700">
                            Gift A School Kit Today
                        </a>
                        {/* <p className="mt-8 text-xs text-gray-500">
                            Partner Charities
                        </p>
                        <div className="flex flex-row mt-3">
                            <div className="basis-1/6 w-10 h-10 ">
                                <img src="https://schoolkit-frontend.vercel.app/_next/image?url=http%3A%2F%2Fthehelpmatesfoundation.com%2Fwp-content%2Fuploads%2F2016%2F08%2Fv2vX4NXz-e1583002125503.jpg&w=1920&q=75" 
                                alt="" className="w-16 mr-2"/>
                            </div>
                            <div className="basis-1/6 w-10 h-10">
                                <img src="https://schoolkit-frontend.vercel.app/_next/image?url=http%3A%2F%2Fthehelpmatesfoundation.com%2Fwp-content%2Fuploads%2F2016%2F08%2Fv2vX4NXz-e1583002125503.jpg&w=1920&q=75" alt="" className="w-16 " />
                            </div>
                        </div> */}
                    </div>
                
                {/* images */}
                    <div className='2xl:ml-44' >
                        <div className="mt-10">
                        <div aria-hidden="true" className="pointer-events-none lg:absolute lg:inset-y-0 xl:max-w-7xl lg:mx-auto lg:w-full">
                            <div className="absolute transform sm:left-1/2 sm:top-0 sm:translate-x-8 lg:left-1/2 lg:top-1/2 lg:-translate-y-1/2 lg:translate-x-8">
                            <div className="flex items-center space-x-6 lg:space-x-8">
                                <div className="flex-shrink-0 grid grid-cols-1 gap-y-6 lg:gap-y-8">
                                <div className="w-44 h-64 rounded-lg overflow-hidden sm:opacity-0 lg:opacity-100">
                                    <img src="https://images.pexels.com/photos/5905497/pexels-photo-5905497.jpeg?cs=srgb&dl=pexels-katerina-holmes-5905497.jpg&fm=jpg" 
                                    alt="" className="w-full h-full object-center object-cover" />
                                </div>
                                <div className="w-44 h-64 rounded-lg overflow-hidden">
                                    <img src="https://thehelpmatesfoundation.com/wp-content/uploads/2020/02/69407463_1197609993760519_1479813965176373248_n-780x450.jpg" 
                                    alt="" className="w-full h-full object-center object-cover"/>
                                </div>
                                </div>
                                <div className="flex-shrink-0 grid grid-cols-1 gap-y-6 lg:gap-y-8">
                                <div className="w-44 h-64 rounded-lg overflow-hidden">
                                    <img src="http://thehelpmatesfoundation.com/wp-content/uploads/2020/02/IMG_5229-1024x683.jpg" alt="" className="w-full h-full object-center object-cover"/>
                                </div>
                                <div className="w-44 h-64 rounded-lg overflow-hidden">
                                    <img src="https://cdn.pixabay.com/photo/2018/02/07/18/30/people-3137672_960_720.jpg" alt="" className="w-full h-full object-center object-cover"/>
                                </div>
                                <div className="w-44 h-64 rounded-lg overflow-hidden">
                                    <img src="https://cdn.pixabay.com/photo/2014/06/26/12/04/students-377789_960_720.jpg" alt="" className="w-full h-full object-center object-cover"/>
                                </div>
                                </div>
                                <div className="flex-shrink-0 grid grid-cols-1 gap-y-6 lg:gap-y-8">
                                <div className="w-44 h-64 rounded-lg overflow-hidden">
                                    <img src="https://images.unsplash.com/photo-1610500795224-fb86b02926d7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80" 
                                    alt="" className="w-full h-full object-center object-cover"/>
                                </div>
                                <div className="w-44 h-64 rounded-lg overflow-hidden">
                                    <img src="https://cdn.pixabay.com/photo/2021/10/28/13/44/children-6749774_960_720.jpg" alt="" className="w-full h-full object-center object-cover"/>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Hero;