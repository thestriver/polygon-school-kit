import React, { useEffect, useState, useRef } from "react";
import { ethers } from "ethers";
import Head from "next/head";
import Hero from "./hero";
import Givers  from "./givers";
import Newsletter from "./newsletter";
import  Goal  from "./goal";
import Footer from "./footer";
import Dreams from "./dreams";
import MaticPayment from "../components/donate-onboard";

export default function Home() {
  const [maticPrice, setMaticPrice] = useState(0.8)
  const [numMatic, SetNumMatic] = useState('25')

  const scrollToDiv = (ref) => window.scrollTo({ top: ref.current.offsetTop, behavior: 'smooth' });
  const el1 = useRef();
  const el2 = useRef();

  //contract address
  if(typeof window !== "undefined"){
    const { ethereum } = window;
  }
  function getLatestPrice(){
    //get latest price
    const aggregatorV3InterfaceABI = [{ "inputs": [], "name": "decimals", "outputs": [{ "internalType": "uint8", "name": "", "type": "uint8" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "description", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "uint80", "name": "_roundId", "type": "uint80" }], "name": "getRoundData", "outputs": [{ "internalType": "uint80", "name": "roundId", "type": "uint80" }, { "internalType": "int256", "name": "answer", "type": "int256" }, { "internalType": "uint256", "name": "startedAt", "type": "uint256" }, { "internalType": "uint256", "name": "updatedAt", "type": "uint256" }, { "internalType": "uint80", "name": "answeredInRound", "type": "uint80" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "latestRoundData", "outputs": [{ "internalType": "uint80", "name": "roundId", "type": "uint80" }, { "internalType": "int256", "name": "answer", "type": "int256" }, { "internalType": "uint256", "name": "startedAt", "type": "uint256" }, { "internalType": "uint256", "name": "updatedAt", "type": "uint256" }, { "internalType": "uint80", "name": "answeredInRound", "type": "uint80" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "version", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "stateMutability": "view", "type": "function" }]
    const addr = "0xd0D5e3DB44DE05E9F294BB0a3bEEaF030DE24Ada"
    const provider = new ethers.providers.JsonRpcProvider("https://matic-mumbai.chainstacklabs.com")
    const priceFeed = new ethers.Contract(addr, aggregatorV3InterfaceABI, provider)
    priceFeed.latestRoundData()
        .then((roundData) => {
            // Do something with roundData
            const answered = Number(ethers.utils.formatUnits((roundData.answer))) * 1e10
            setMaticPrice(answered)
            const numAnswer = (20 /  Number(answered)).toFixed(2)
            SetNumMatic(numAnswer)
         })
 }

 useEffect(() => { 
   getLatestPrice()
 // eslint-disable-next-line react-hooks/exhaustive-deps
 }, [])

  return (
    <>
      <Head>
          <title>Donate a SchoolKit</title>
          <link rel="icon" href="/favicon.ico" />
      </Head>
      <Hero  reference={el1} click={()=> scrollToDiv(el2)}/>
      <section className="py-20">
        <div className="2xl:mx-44 xl:mx-16 lg:mx-10 px-4 sm:px-6 ease-in-out duration-300">
          <div className="flex flex-wrap ">
            <Dreams />
            {/* Donate section */}
            <div className="w-full lg:w-1/2 px-8  2xl:pl-16 lg:pl-16 pl-4 ">
              <ul className="space-y-12">
                <li className="flex -mx-4">
                  <div className="px-4">
                    <span className="flex w-16 h-16 mx-auto items-center justify-center text-2xl font-bold font-heading rounded-full bg-blue-600 text-white">
                    $20
                    </span>
                  </div>
                  <div className="">
                    
                    
                    <MaticPayment numMatic={numMatic} reference={el2}  />                
                  

                </div>
                </li>
              </ul>
            
            </div>
          </div>
        </div>
      </section>
     
      <Goal maticPrice={maticPrice} />
      <Givers />
      <Newsletter />
      <Footer />
    </>
  );
}